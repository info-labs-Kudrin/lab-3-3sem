﻿#include "pch.h"
#include "CppUnitTest.h"
#include "../LAB3_3sem/Node.h"
#include "../LAB3_3sem/Graph.h"

#include <map>

#define forn(i, j, k) for(int i = j; i < k; i++)

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace LAB3Tests
{
	TEST_CLASS(NodeTests)
	{
	public:
		
		TEST_METHOD(InitTests)
		{
			Node<int> a, b(2);
			Assert::IsTrue(b.info == 2 && b.isMarked == 0);
			Node<Node<int>> c(a);
		}
	};

	TEST_CLASS(GraphTests)
	{
	public:

		TEST_METHOD(AddNodeTest)
		{
			Graph<int> a;
			Assert::IsTrue(a.edgesAllRight());
			Assert::IsTrue(a.addNode() == 0);
			Assert::IsTrue(a.addNode() == 1);
			Assert::IsTrue(a.addNode() == 2);
			Assert::IsTrue(a.edgesAllRight());
		}
		TEST_METHOD(GetNodeTest)
		{
			Graph<int> a;
			a.addNode();
			a.addNode();
			a.addNode();
			a[1].info = 19;
			a[0].isMarked = 1;
			a[2].nodeWeight = 2;
			Assert::IsTrue(a[1].info == 19 && a[0].isMarked == 1 && a[2].nodeWeight == 2);

			Assert::ExpectException<std::exception>([&a]() {a[3]; });
			Assert::ExpectException<std::exception>([&a]() {a[-1]; });
			Assert::IsTrue(a.edgesAllRight());
		}
		TEST_METHOD(EdgesTest)
		{
			Graph<int> a;
			a.addNode();
			a.addNode();
			a.addNode();
			a.setEdge(0, 1, 2);
			Assert::IsTrue(a.edgesAllRight());
			a.setEdge(0, 0);
			Assert::IsTrue(a.edgesAllRight());
			std::vector<std::pair<int, int>> c = a.getConnections(0), d = a.getConnections(1);
			Assert::IsTrue(a.edgesAllRight());
			Assert::IsTrue(c.size() == 2 && c[0].first == 1 && c[0].second == 2 && c[1].first == 0 && c[1].second == 1 && d.size() == 0);

			Assert::ExpectException<std::exception>([&a]() {a.setEdge(-1, 1); });
			Assert::ExpectException<std::exception>([&a]() {a.setEdge(3, 1); });
			Assert::ExpectException<std::exception>([&a]() {a.setEdge(1, 3); });
			Assert::ExpectException<std::exception>([&a]() {a.setEdge(1, -1); });
			Assert::ExpectException<std::exception>([&a]() {a.getConnections(3); });
			Assert::ExpectException<std::exception>([&a]() {a.getConnections(-1); });
		}
		TEST_METHOD(DijkstraTest)
		{
			Graph<int> a;
			a.addNode();
			pair<vector<unsigned int>, vector<unsigned int>> r = a.Dijkstra(0, 0);
			Assert::IsTrue(r.first.size() == 1 && r.first[0] == 0);
			Assert::IsTrue(r.second.size() == 1 && r.first[0] == 0);
			a.addNode();
			a.addNode();
			a.addNode();
			a.setEdge(0, 2);
			a.setEdge(2, 3);
			r = a.Dijkstra(0, 1);
			Assert::IsTrue(r.first.size() == 4 && r.first[0] == 0 && r.first[1] == INT_MAX && r.first[2] == 1 && r.first[3] == 2);
			Assert::IsTrue(r.second.size() == 0);
			a = Graph<int>();
			a.addNode();
			a.addNode();
			a.addNode();
			a.addNode();
			a.addNode();
			a.setEdge(0, 1, 1);
			a.setEdge(0, 2, 3);
			a.setEdge(0, 3, 10);
			a.setEdge(0, 4, 1);
			a.setEdge(1, 0, 1);
			a.setEdge(1, 2, 1);
			a.setEdge(2, 0, 3);
			a.setEdge(2, 1, 1);
			a.setEdge(2, 3, 1);
			a.setEdge(3, 0, 10);
			a.setEdge(3, 2, 1);
			a.setEdge(4, 1, 1);
			r = a.Dijkstra(0, 3);
			Assert::IsTrue(r.first[0] == 0 && r.first[1] == 1 && r.first[2] == 2 && r.first[3] == 3 && r.first[4] == 1);
			Assert::IsTrue(r.second.size() == 4 && r.second[0] == 0 && r.second[1] == 1 && r.second[2] == 2 && r.second[3] == 3);
			r = a.Dijkstra(2, 4);
			Assert::IsTrue(r.first[0] == 2 && r.first[1] == 1 && r.first[2] == 0 && r.first[3] == 1 && r.first[4] == 3);
			Assert::IsTrue(r.second.size() == 4 && r.second[0] == 2 && r.second[1] == 1 && r.second[2] == 0 && r.second[3] == 4);

			Assert::ExpectException<exception>([&a]() {a.Dijkstra(0, 10); });
			Assert::ExpectException<exception>([&a]() {a.Dijkstra(10, 0); });
			Assert::ExpectException<exception>([&a]() {a.Dijkstra(10, 10); });
			a.setEdge(0, 1, -1);
			Assert::ExpectException<exception>([&a]() {a.Dijkstra(0, 0); });
		}
		TEST_METHOD(GetConnectionsTest)
		{
			Graph<int> a;
			Assert::ExpectException<exception>([&a]() {a.getConnections(0); });
			a.addNode();
			Assert::IsTrue(a.getConnections(0).size() == 0);
			a.addNode();
			a.addNode();
			Assert::IsTrue(a.getConnections(0).size() == 0);
			a.setEdge(0, 1);
			Assert::IsTrue(a.getConnections(0).size() == 1 && a.getConnections(0)[0].first == 1 && a.getConnections(0)[0].second == 1);
			a.setEdge(0, 2, -2);
			Assert::IsTrue(a.getConnections(0).size() == 2 && a.getConnections(0)[0].first == 1 && a.getConnections(0)[0].second == 1 && a.getConnections(0)[1].first == 2 && a.getConnections(0)[1].second == -2);
			Assert::IsTrue(a.edgesAllRight());

			a.removeNode(1);
			Assert::IsTrue(a.getConnections(0).size() == 1 && a.getConnections(0)[0].first == 1 && a.getConnections(0)[0].second == -2);
		}
		TEST_METHOD(RemoveEdgeTest)
		{
			Graph<int> a;
			a.addNode();
			a.addNode();
			a.addNode();
			a.setEdge(0, 1);
			a.setEdge(0, 2);
			Assert::IsTrue(a.edgesAllRight());
			a.removeEdge(0, 1);
			Assert::IsTrue(a.edgesAllRight());
			Assert::IsTrue(a.getConnections(0).size() == 1 && a.getConnections(0)[0].first == 2 && a.getConnections(0)[0].second == 1);
			Assert::ExpectException<exception>([&a]() {a.removeEdge(0, 1); });
			Assert::ExpectException<exception>([&a]() {a.removeEdge(0, 3); });
		}
		TEST_METHOD(TopologicallySortTest)
		{
			Assert::ExpectException<exception>([]() {
				Graph<int> a;
				a.addNode();
				a.addNode();
				a.setEdge(0, 1);
				a.setEdge(1, 0);
				a.topologicallySort();
				});
			Graph<int> a;
			a.addNode(3);
			a[0].info = 1;
			a[1].info = 2;
			a[2].info = 3;
			a.setEdge(2, 1, 1);
			a.setEdge(2, 0, 2);
			a.setEdge(1, 0, 3);
			Assert::IsTrue(a.edgesAllRight());
			vector<unsigned int> r = a.topologicallySort();
			Assert::IsTrue(r[0] == 2 && r[1] == 1 && r[2] == 0);
			vector<pair<int, int>> con = a.getConnections(0);
			Assert::IsTrue(con.size() == 2 && ((con[0].first == 1 && con[0].second == 1 && con[1].first == 2 && con[1].second == 2) || (con[1].first == 1 && con[1].second == 1 && con[0].first == 2 && con[0].second == 2)));
			con = a.getConnections(1);
			Assert::IsTrue(con.size() == 1 && con[0].first == 2 && con[0].second == 3);
			con = a.getConnections(2);
			Assert::IsTrue(con.size() == 0);
			Assert::IsTrue(a[0].info == 3 && a[1].info == 2 && a[2].info == 1);
			Assert::IsTrue(a.edgesAllRight());

			a = Graph<int>();
			a.addNode(3);
			a[0].info = 1;
			a[1].info = 2;
			a[2].info = 3;
			a.setEdge(1, 0, 1);
			a.setEdge(1, 2, 2);
			a.setEdge(0, 2, 3);
			r = a.topologicallySort();
			Assert::IsTrue(r[0] == 1 && r[1] == 0 && r[2] == 2);
			con = a.getConnections(0);
			Assert::IsTrue(con.size() == 2 && ((con[0].first == 1 && con[0].second == 1 && con[1].first == 2 && con[1].second == 2) || (con[1].first == 1 && con[1].second == 1 && con[0].first == 2 && con[0].second == 2)));
			con = a.getConnections(1);
			Assert::IsTrue(con.size() == 1 && con[0].first == 2 && con[0].second == 3);
			con = a.getConnections(2);
			Assert::IsTrue(con.size() == 0);
			Assert::IsTrue(a[0].info == 2 && a[1].info == 1 && a[2].info == 3);
			Assert::IsTrue(a.edgesAllRight());

			a = Graph<int>();
			a.addNode(3);
			a[0].info = 1;
			a[1].info = 2;
			a[2].info = 3;
			a.setEdge(1, 0, 2);
			a.setEdge(1, 2, 1);
			a.setEdge(2, 0, 3);
			r = a.topologicallySort();
			Assert::IsTrue(r[0] == 2 && r[1] == 0 && r[2] == 1);
			con = a.getConnections(0);
			Assert::IsTrue(con.size() == 2 && 
				((con[0].first == 1 && con[0].second == 1 && con[1].first == 2 && con[1].second == 2) || 
					(con[1].first == 1 && con[1].second == 1 && con[0].first == 2 && con[0].second == 2)));
			con = a.getConnections(1);
			Assert::IsTrue(con.size() == 1 && con[0].first == 2 && con[0].second == 3);
			con = a.getConnections(2);
			Assert::IsTrue(con.size() == 0);
			Assert::IsTrue(a[0].info == 2 && a[1].info == 3 && a[2].info == 1);
			Assert::IsTrue(a.edgesAllRight());

			a = Graph<int>();
			a.addNode(8);
			a.setEdge(1, 4);
			a.setEdge(2, 1);
			a.setEdge(2, 3);
			a.setEdge(3, 0);
			a.setEdge(3, 1);
			a.setEdge(5, 4);
			a.setEdge(5, 6);
			a.topologicallySort();
			forn(i, 0, 8)
			{
				vector<pair<int, int>> con = a.getConnections(i);
				forn(j, 0, con.size())
					Assert::IsTrue(con[j].first > i);
			}
			Assert::IsTrue(a.edgesAllRight());

			a = Graph<int>();
			a.addNode(3);
			a.setEdge(0, 2);
			a.setEdge(0, 1);
			a.setEdge(1, 2);
			a.topologicallySort();
		}
		TEST_METHOD(RemoveNodeTest)
		{
			Graph<int> a;
			a.addNode(4);
			a[0].info = 0; a[1].info = 1; a[2].info = 2; a[3].info = 3;
			forn(i, 0, 4)
				forn(j, 0, 4)
				a.setEdge(i, j);
			Assert::IsTrue(a.edgesAllRight());
			a.removeNode(1);
			Assert::IsTrue(a[0].info == 0 && a[1].info == 2 && a[2].info == 3);
			Assert::ExpectException<exception>([&a]() {a[3]; });
			Assert::IsTrue(a.edgesAllRight());

			//May collapse if method of storing edges is changed
			forn(i, 0, 3)
			{
				auto v = a.getConnections(i);
				Assert::IsTrue(v.size() == 3);
				forn(j, 0, 3)
					Assert::IsTrue(v[j].first == j);
			}
		}
		TEST_METHOD(GetSCCTest)
		{
			Graph<int> a;
			a.getSCC();
			a.addNode(5);
			a.setEdge(1, 2);
			a.setEdge(2, 3);
			a.setEdge(3, 4);
			a.setEdge(4, 1);
			a.setEdge(0, 1);
			vector<vector<unsigned int>> r = a.getSCC();
			Assert::IsTrue(r.size() == 2);
			if (r[0].size() == 4)
				swap(r[0], r[1]);
			Assert::IsTrue(r[0].size() == 1 && r[1].size() == 4);
			Assert::IsTrue(r[0][0] == 0);
			map<int, bool> s;
			for (int i = 1; i < 5; i++)
				s[i] = 0;
			for (int i = 0; i < 4; i++)
				s[r[1][i]] = 1;
			for (int i = 1; i < 5; i++)
				Assert::IsTrue(s[i]);
		}
		TEST_METHOD(GetCondensationTest)
		{
			Graph<int> a;
			a.addNode(5);
			a.setEdge(1, 2);
			a.setEdge(2, 3);
			a.setEdge(3, 4);
			a.setEdge(4, 1);
			a.setEdge(0, 1);
			Graph<vector<unsigned int>> *c = a.getCondensation();
			Assert::IsTrue(c->size() == 2);
			c->topologicallySort();
			Assert::IsTrue(c->getConnections(0)[0].first == 1);
			Assert::IsTrue((*c)[0].info.size() == 1 && (*c)[0].info[0] == 0);
			Assert::IsTrue((*c)[1].info.size() == 4);
			map<int, bool> met;
			for (int i = 1; i < 5; i++)
				met[i] = 0;
			for (int i = 0; i < 4; i++)
				met[(*c)[1].info[i]] = 1;
			for (int i = 1; i < 5; i++)
				Assert::IsTrue(met[i]);

			delete c;
		}
		TEST_METHOD(MinimumVertexCoverTest)
		{
			Graph<int> a;
			Assert::IsTrue(a.getMinimumVertexCover().size() == 0);
			a.addNode(3);
			a.setEdge(0, 1);
			a.setEdge(0, 2);
			vector<unsigned int> r = a.getMinimumVertexCover();
			Assert::IsTrue(r.size() == 1 && r[0] == 0);
			a.addNode();
			a.setEdge(1, 3);
			r = a.getMinimumVertexCover();
			Assert::IsTrue(r.size() == 2 && ((r[0] == 0 && r[1] == 1) || (r[0] == 1 && r[1] == 0)));

			Graph<int> b;
			b.addNode(3);
			b.setEdge(0, 1);
			b.setEdge(1, 0);
			b.setEdge(1, 2);
			r = b.getMinimumVertexCover();
			Assert::IsTrue(r.size() == 1 && r[0] == 1);

			Graph<int> c;
			c.addNode(3);
			c.setEdge(2, 1);
			c.setEdge(1, 2);
			c.setEdge(2, 0);
			r = c.getMinimumVertexCover();
			Assert::IsTrue(r.size() == 1 && r[0] == 2);
		}
	};
}