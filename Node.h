#pragma once

#include<vector>

template <class T>
class Node
{
public:
	T info;
	int nodeWeight;
	bool isMarked;

	Node();
	Node(T info);
};

template<class T>
inline Node<T>::Node() : Node(T()) {};

template<class T>
inline Node<T>::Node(T info)
{
	this->info = info; 
	nodeWeight = 0; 
	isMarked = 0; 
};