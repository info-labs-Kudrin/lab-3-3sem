#pragma once

#include "Graph.h"

#include <vector>

namespace Graphs {

	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for FormerGraphForm
	/// </summary>
	public ref class FormerGraphForm : public System::Windows::Forms::Form
	{
		Graph<int>* theGraph;
		vector<int>* nodes_x, * nodes_y, * colors;
	private: System::Windows::Forms::PictureBox^ pictureBox1;

	public:
		FormerGraphForm(void)
		{
			InitializeComponent();
			nodes_x = nullptr;
			nodes_y = nullptr;
			colors = nullptr;
			theGraph = new Graph<int>();
		}
		void setGraph(Graph<int>* a, vector<int>* nx, vector<int> *ny)
		{			
			size_t n = a->size();
			delete theGraph;
			theGraph = new Graph<int>();
			theGraph->addNode(n);
			for (unsigned int i = 0; i < n; i++)
			{
				vector<pair<int, int>> cons = a->getConnections(i);
				size_t s = cons.size();
				for (unsigned int j = 0; j < s; j++)
					theGraph->setEdge(i, cons[j].first, cons[j].second);
			}

			nodes_x = new vector<int>(n);
			nodes_y = new vector<int>(n);
			colors = new vector<int>(n);
			for (int i = 0; i < n; i++)
			{
				(*nodes_x)[i] = (*nx)[i];
				(*nodes_y)[i] = (*ny)[i]; 
				(*colors)[i] = 3506437888;
			}
			this->pictureBox1->Refresh();
		}


	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FormerGraphForm()
		{
			if (components)
			{
				delete components;
			}
			if (nodes_x)
				delete nodes_x;
			if (nodes_y)
				delete nodes_y;
			if (theGraph)
				delete theGraph;
			nodes_x = nodes_y = nullptr;
			theGraph = nullptr;
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(10, 10);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(0);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(1024, 834);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &FormerGraphForm::pictureBox1_Paint);
			// 
			// FormerGraphForm
			// 
			this->ControlBox = false;
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Maroon;
			this->ClientSize = System::Drawing::Size(1060, 853);
			this->Controls->Add(this->pictureBox1);
			this->Location = System::Drawing::Point(-100, -100);
			this->MaximumSize = System::Drawing::Size(1060, 900);
			this->MinimumSize = System::Drawing::Size(1060, 900);
			this->Name = L"FormerGraphForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void pictureBox1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) {
		int n = nodes_x->size();
		SolidBrush^ drawBrush_text = gcnew SolidBrush(Color::Black);
		Drawing::Font^ drawFont = gcnew Drawing::Font("Arial", 12);
		StringFormat^ drawFormat = gcnew StringFormat();

		for (int i = 0; i < n; i++)
		{
			int xcur = (*nodes_x)[i], ycur = (*nodes_y)[i];
			SolidBrush^ drawBrush = gcnew SolidBrush(Color::FromArgb((*colors)[i]));
			e->Graphics->FillEllipse(drawBrush, xcur, ycur, 30, 30);

			String^ s = ""; int r = i;
			if (i == 0)
				s = "0";
			while (r != 0) {
				s = r % 10 + s;
				r /= 10;
			}

			e->Graphics->DrawString(s, drawFont, drawBrush_text, xcur + 4, ycur + 4, drawFormat);

			vector<pair<int, int>> cons = theGraph->getConnections(i);
			int sz = cons.size();
			SolidBrush^ drawBrush_text = gcnew SolidBrush(Color::LightSeaGreen);
			for (int j = 0; j < sz; j++)
			{
				int xsec = (*nodes_x)[cons[j].first], ysec = (*nodes_y)[cons[j].first];
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 14, ycur + 15, xsec + 14, ysec + 15);
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 15, ycur + 15, xsec + 15, ysec + 15);
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 15, ycur + 16, xsec + 15, ysec + 16);

				s = ""; r = cons[j].second;
				if (r == 0)
					s = "0";
				while (r != 0) {
					s = r % 10 + s;
					r /= 10;
				}
				e->Graphics->DrawString(s, drawFont, drawBrush_text, xsec + 13 + (xcur - xsec) * 0.1, ysec + 11 + (ycur - ysec) * 0.1, drawFormat);
			}
		}
	}
	};
}
