#pragma once
#include<vector>
#include<list>
#include<exception>
#include<stack>
#include<set>

#include"Node.h"

template<class T>
class Graph
{
private:
	std::vector<Node<T>*> nodes;
	
	//number in the vector refers to the first node (from), 
	//first element in pairs refers to the second node (to),
	//second element in pairs referse to the edge's weight
	//multiple edges are not supported
	std::vector<std::list<std::pair<unsigned int, int>>> adjacencyLists;
	std::vector<std::list<std::pair<unsigned int, int>>> adjacencyLists_reversed;

	//First met not marked node is placed in v
	bool eachNodeMarked(unsigned int &v);

	//Self-explanotary name
	bool has_negative_edges();

	//Finds all nodes which can be reached from nd and sets true there
	std::vector<bool> getReachedNodes(unsigned int nd);

	//Finds all nodes from which nd can be reached and sets true there
	std::vector<bool> getReachedNodes_from(unsigned int nd);

	//Multiplies current disjunctive normal form (DNF) by other + curV
	void multiply(std::vector<std::set<unsigned int>>& a, std::list<std::pair<unsigned int, int>> &other, unsigned int curV);

	//Checks if f is shorted than s
	bool isIn(std::set<unsigned int>& f, std::set<unsigned int>& s);

	//Shortens DNF
	void shorten(std::vector<std::set<unsigned int>>& a);

public:
	//Returns number of nodes in graph
	unsigned int size();

	//resets every node to not marked
	void resetMarked();

	//Adds new empty node, returns ints number
	int addNode();

	//Adds num nodes
	void addNode(unsigned int num);

	//In returned vector value val on i position means node with former number i now has now number val
	std::vector<unsigned int> topologicallySort();

	//Adds new (weighted) edge from node1 to node2 or sets weight to the exsisting edge
	void setEdge(unsigned int node1, unsigned int node2, unsigned int weight = 1);

	//Removes an edge from the graph
	void removeEdge(unsigned int node1, unsigned int node2);

	//Removes a node from the graph
	void removeNode(unsigned int nd);

	//Returns reference to the node
	Node<T>& operator[](unsigned int node);

	//Returns vector of nodes one can get to from the node and distances to them
	std::vector<std::pair<int, int>> getConnections(unsigned int node);

	//First is distance to every node (if infinite, then it is INT_MAX), second is the shortest way from node 1 to node 2 (node1->...->node2)
	std::pair<std::vector<unsigned int>, std::vector<unsigned int>> Dijkstra(unsigned int node1, unsigned int node2);

	//returns vectors of numbers of nodes which are strongly connected (strongly connected components)
	std::vector<std::vector<unsigned int>> getSCC();

	//Returns graph's condensation, in stored vectors -- original nodes. Do not forget to delete the resulting graph
	Graph<std::vector<unsigned int>>* getCondensation();

	//Returns nodes which are in a minimum (related to the number of vertices) vertex cover
	std::vector<unsigned int> getMinimumVertexCover();


	~Graph();

	//
	bool edgesAllRight();
};

template<class T>
inline unsigned int Graph<T>::size()
{
	return nodes.size();
}

template<class T>
inline void Graph<T>::resetMarked()
{
	int sz = nodes.size();
	for (int i = 0; i < sz; i++)
		nodes[i]->isMarked = 0;
}

template<class T>
inline bool Graph<T>::eachNodeMarked(unsigned int &v)
{
	int sz = nodes.size();
	for (int i = 0; i < sz; i++)
		if (!nodes[i]->isMarked)
		{
			v = i;
			return false;
		}
	return true;
}

template<class T>
inline bool Graph<T>::has_negative_edges()
{
	int sz = adjacencyLists.size();
	for (int i = 0; i < sz; i++)
	{
		auto cur = adjacencyLists[i].begin(), fin = adjacencyLists[i].end();
		while (cur != fin)
		{
			if (cur->second < 0)
				return true;
			cur++;
		}
	}
	return false;
}

template<class T>
inline std::vector<bool> Graph<T>::getReachedNodes(unsigned int nd)
{
	std::vector<bool> reaches(nodes.size(), 0);

	//dfs
	std::stack<unsigned int> st;
	st.push(nd);
	while (!st.empty())
	{
		unsigned int v1 = st.top();
		st.pop();
		auto cur = adjacencyLists[v1].begin(), fin = adjacencyLists[v1].end();
		while (cur != fin)
		{
			if (!reaches[cur->first])
			{
				reaches[cur->first] = 1;
				st.push(cur->first);
			}
			cur++;
		}
	}

	return reaches;
}

template<class T>
inline std::vector<bool> Graph<T>::getReachedNodes_from(unsigned int nd)
{
	std::vector<bool> isReachedFrom(nodes.size(), 0);

	//dfs
	std::stack<unsigned int> st;
	st.push(nd);
	while (!st.empty())
	{
		unsigned int v1 = st.top();
		st.pop();
		auto cur = adjacencyLists_reversed[v1].begin(), fin = adjacencyLists_reversed[v1].end();
		while (cur != fin)
		{
			if (!isReachedFrom[cur->first])
			{
				isReachedFrom[cur->first] = 1;
				st.push(cur->first);
			}
			cur++;
		}
	}
	return isReachedFrom;
}

template<class T>
inline void Graph<T>::multiply(std::vector<std::set<unsigned int>>& a, std::list<std::pair<unsigned int, int>>& other, unsigned int curV)
{
	size_t sz = a.size();

	std::vector<std::set<unsigned int>> b;
	auto cur = other.begin(), fin = other.end();
	while (cur != fin)
	{
		for (unsigned int i = 0; i < sz; i++)
		{
			std::set<unsigned int> curSet = a[i];
			curSet.insert(cur->first);
			b.push_back(curSet);
		}
		cur++;
	}
	for (unsigned int i = 0; i < sz; i++)
	{
		std::set<unsigned int> curSet = a[i];
		curSet.insert(curV);
		b.push_back(curSet);
	}
	a = b;
}

template<class T>
inline bool Graph<T>::isIn(std::set<unsigned int>& f, std::set<unsigned int>& s)
{
	auto cur = f.begin(), fin = f.end();
	while (cur != fin)
	{
		if (s.find(*cur) == s.end())
			return false;
		cur++;
	}
	return true;
}

template<class T>
inline void Graph<T>::shorten(std::vector<std::set<unsigned int>>& a)
{
	size_t sz = a.size();
	for (unsigned int i = 0; i < sz; i++)
	{
		for (unsigned int j = i + 1; j < sz; j++)
		{
			if (isIn(a[j], a[i]))
			{
				a.erase(a.begin() + i);
				sz--;
				i--;
				break;
			}
			if (isIn(a[i], a[j]))
			{
				a.erase(a.begin() + j);
				sz--;
				j--;
			}
		}
	}
}

template<class T>
inline int Graph<T>::addNode()
{
	Node<T>* nw = new Node<T>();
	nodes.push_back(nw);
	adjacencyLists.push_back(std::list<std::pair<unsigned int, int>>());
	adjacencyLists_reversed.push_back(std::list<std::pair<unsigned int, int>>());
	return nodes.size() - 1;
}

template<class T>
inline void Graph<T>::addNode(unsigned int num)
{
	for (unsigned int i = 0; i < num; i++)
		this->addNode();
}

template<class T>
inline std::vector<unsigned int> Graph<T>::topologicallySort()
{
	resetMarked();
	unsigned int n = nodes.size();
	std::vector<unsigned int> res(n), res_inv(n);
	
	unsigned int v = -1;
	int count = 0;
	bool* processed = new bool[n];
	bool* added_edges = new bool[n];
	for (unsigned int i = 0; i < n; i++) {
		processed[i] = false;
		added_edges[i] = false;
	}
	//dfs while can
	while (!eachNodeMarked(v))
	{
		std::stack<unsigned int> st;
		st.push(v);
		//dfs
		while (!st.empty())
		{
			v = st.top();
			bool added = 0;
			if (!nodes[v]->isMarked)
			{
				nodes[v]->isMarked = 1;

				auto cur = adjacencyLists[v].begin(), fin = adjacencyLists[v].end();
				while (cur != fin)
				{
					unsigned int cr = cur->first;
					if (nodes[cr]->isMarked)
					{
						if (!processed[cr])
							throw std::exception("A cycle found");
					}
					else if (!added_edges[cr])
					{
						st.push(cr);
						added_edges[cr] = 1;
						added = 1;
					}
					cur++;
				}
			}
			if (!added)
			{
				st.pop();
				processed[v] = true;
				res_inv[count] = v;
				count++;
			}
		}
	}
	delete[] processed;

	std::reverse(res_inv.begin(), res_inv.end());
	for (unsigned int i = 0; i < n; i++)
		res[res_inv[i]] = i;
	std::vector<Node<T>*> nwNodes(n);
	std::vector<std::list<std::pair<unsigned int, int>>> nwLists(n), nwLists_reversed(n);
	for (unsigned int i = 0; i < n; i++)
	{
		nwNodes[i] = nodes[res_inv[i]];
		auto cur = adjacencyLists[res_inv[i]].begin(), fin = adjacencyLists[res_inv[i]].end();
		while (cur != fin)
		{
			nwLists[i].push_back({ res[cur->first], cur->second });
			cur++;
		}
		cur = adjacencyLists_reversed[res_inv[i]].begin(), fin = adjacencyLists_reversed[res_inv[i]].end();
		while (cur != fin)
		{
			nwLists_reversed[i].push_back({ res[cur->first], cur->second });
			cur++;
		}
	}
	this->adjacencyLists = nwLists;
	this->adjacencyLists_reversed = nwLists_reversed;
	this->nodes = nwNodes;

	return res;
}

template<class T>
inline void Graph<T>::setEdge(unsigned int node1, unsigned int node2, unsigned int weight)
{
	if (node1 >= nodes.size() || node2 >= nodes.size())
		throw std::exception("Node doees not exsist");
	int sz = adjacencyLists[node1].size();
	auto /*std::list<std::pair<int, int>>::iterator*/ cur = adjacencyLists[node1].begin(), fin = adjacencyLists[node1].end();
	bool exists = 0;
	while (cur != fin)
	{
		if (cur->first == node2)
		{
			cur->second = weight;
			exists = 1;
			break;
		}
		cur++;
	}
	if (!exists)
	{
		adjacencyLists[node1].push_back({ node2, weight });
		adjacencyLists_reversed[node2].push_back({ node1, weight });
		return;
	}
	cur = adjacencyLists_reversed[node2].begin(), fin = adjacencyLists_reversed[node2].end();
	while (cur != fin)
	{
		if (cur->first == node1)
		{
			cur->second = weight;
			return;
		}
		cur++;
	}
}

template<class T>
inline void Graph<T>::removeEdge(unsigned int node1, unsigned int node2)
{
	if (node1 >= nodes.size() || node2 >= nodes.size())
		throw std::exception("Node does not exist");
	auto cur = adjacencyLists[node1].begin(), fin = adjacencyLists[node1].end();
	while (cur != fin && cur->first != node2)
		cur++;
	if (cur == fin)
		throw std::exception("Edge does not exist");
	adjacencyLists[node1].erase(cur);

	cur = adjacencyLists_reversed[node2].begin(), fin = adjacencyLists_reversed[node2].end();
	while (cur != fin && cur->first != node1)
		cur++;
	adjacencyLists_reversed[node2].erase(cur);
}

template<class T>
inline void Graph<T>::removeNode(unsigned int nd)
{
	if (nd >= nodes.size())
		throw std::exception("Node does not exist");
	nodes.erase(nodes.begin() + nd);
	adjacencyLists.erase(adjacencyLists.begin() + nd);
	adjacencyLists_reversed.erase(adjacencyLists_reversed.begin() + nd);

	int sz = nodes.size();
	for (int i = 0; i < sz; i++) {
		auto cur = adjacencyLists[i].begin(), fin = adjacencyLists[i].end();
		while (cur != fin)
		{
			if (cur->first == nd)
			{
				std::list<std::pair<unsigned int, int>>::iterator removed = cur;
				cur++;
				adjacencyLists[i].erase(removed);
				continue;
			}
			else if (cur->first > nd)
				cur->first--;
			cur++;
		}

		cur = adjacencyLists_reversed[i].begin(), fin = adjacencyLists_reversed[i].end();
		while (cur != fin)
		{
			if (cur->first == nd)
			{
				std::list<std::pair<unsigned int, int>>::iterator removed = cur;
				cur++;
				adjacencyLists_reversed[i].erase(removed);
				continue;
			}
			else if (cur->first > nd)
				cur->first--;
			cur++;
		}
	}
}

template<class T>
inline Node<T>& Graph<T>::operator[] (unsigned int node)
{
	if (node >= nodes.size())
		throw std::exception("Node does not exist");
	return *(nodes[node]);
}

template<class T>
inline std::vector<std::pair<int, int>> Graph<T>::getConnections(unsigned int node)
{
	if (node >= nodes.size())
		throw std::exception("Node does not exist");
	return std::vector<std::pair<int, int>>(adjacencyLists[node].begin(), adjacencyLists[node].end());
}

template<class T>
inline std::pair<std::vector<unsigned int>, std::vector<unsigned int>> Graph<T>::Dijkstra(unsigned int node1, unsigned int node2)
{
	if (this->has_negative_edges())
		throw std::exception("There is at least one negative edge");
	if (node1 >= nodes.size() || node2 >= nodes.size())
		throw std::exception("Node does not exist");

	int n = adjacencyLists.size();

	//Preinitializing
	resetMarked();
	nodes[node1]->isMarked = 1;
	std::vector<unsigned int> closest_prev_node(n, -1), distances(n, INT_MAX);
	distances[node1] = 0;
	closest_prev_node[node1] = node1;

	//Dijkstra
	while (true)
	{
		unsigned int v = UINT_MAX, mi = INT_MAX, prev;
		for (int i = 0; i < n; i++)
		{
			if (nodes[i]->isMarked)
			{
				auto curEdge = adjacencyLists[i].begin(), fin = adjacencyLists[i].end();
				while (curEdge != fin)
				{
					if (!nodes[curEdge->first]->isMarked && (v == UINT_MAX || distances[i] + curEdge->second < mi))
					{
						mi = distances[i] + curEdge->second;
						v = curEdge->first;
						prev = i;
					}
					curEdge++;
				}
			}
		}
		if (v == UINT_MAX)
			break;
		distances[v] = mi;
		nodes[v]->isMarked = true;
		closest_prev_node[v] = prev;
	}

	//Getting path
	if (closest_prev_node[node2] != -1)
	{
		std::vector<unsigned int> path = { node2 };
		int i = node2;
		while (i != node1)
		{
			i = closest_prev_node[i];
			path.push_back(i);
		}
		std::reverse(path.begin(), path.end());
		return { distances, path };
	}

	return { distances, {} };
}

template<class T>
inline std::vector<std::vector<unsigned int>> Graph<T>::getSCC()
{
	int n = nodes.size();
	std::vector<std::vector<unsigned int>> res;
	this->resetMarked();
	unsigned int v;

	while (!this->eachNodeMarked(v))
	{
		std::vector<bool> isReachedFrom = getReachedNodes_from(v), reaches = getReachedNodes(v);
		std::vector<unsigned int> scc;
		for (int i = 0; i < n; i++)
			if ((reaches[i] && isReachedFrom[i]) || i == v)
			{
				scc.push_back(i);
				nodes[i]->isMarked = 1;
			}
		res.push_back(scc);
	}
	return res;
}

template<class T>
inline Graph<std::vector<unsigned int>>* Graph<T>::getCondensation()
{
	Graph<std::vector<unsigned int>>* res = new Graph<std::vector<unsigned int>>();
	std::vector<std::vector<unsigned int>> SCCs = this->getSCC();
	int sz = SCCs.size();
	for (int i = 0; i < sz; i++)
	{
		res->addNode();
		(*res)[i].info = SCCs[i];
	}
	for (int i = 0; i < sz; i++)
	{
		std::vector<bool> v = this->getReachedNodes(SCCs[i][0]);
		for (int j = 0; j < sz; j++)
		{
			if (j == i)
				continue;
			if (v[SCCs[j][0]])
				res->setEdge(i, j);
		}
	}
	return res;
}




template<class T>
inline std::vector<unsigned int> Graph<T>::getMinimumVertexCover()
{
	if (nodes.size() == 0)
		return {};
	std::vector<std::set<unsigned int>> dnf(0);
	auto cur = adjacencyLists_reversed[0].begin(), fin = adjacencyLists_reversed[0].end();

	//first node
	dnf.push_back(std::set<unsigned int>());
	dnf[0].insert(0);
	while (cur != fin)
	{
		dnf.push_back(std::set<unsigned int>());
		dnf[dnf.size() - 1].insert(cur->first);
		cur++;
	}

	size_t sz = nodes.size();
	for (unsigned int i = 1; i < sz; i++)
	{
		multiply(dnf, adjacencyLists_reversed[i], i);
		shorten(dnf);
	}
	
	size_t miSz = dnf[0].size(), miI = 0;
	sz = dnf.size();
	for (unsigned int i = 1; i < sz; i++)
		if (dnf[i].size() < miSz)
		{
			miI = i;
			miSz = dnf[i].size();
		}

	return std::vector<unsigned int>(dnf[miI].begin(), dnf[miI].end());
}

template<class T>
inline Graph<T>::~Graph()
{
	int sz = nodes.size();
	for (int i = 0; i < sz; i++)
		delete nodes[i];
}

template<class T>
inline bool Graph<T>::edgesAllRight()
{
	int sz = adjacencyLists.size();
	if (sz != adjacencyLists_reversed.size())
		return false;
	for (int i = 0; i < sz; i++)
	{
		//All from straight exist in reversed
		auto cur = adjacencyLists[i].begin(), fin = adjacencyLists[i].end();
		while (cur != fin)
		{
			auto cur1 = adjacencyLists_reversed[cur->first].begin(), fin1 = adjacencyLists_reversed[cur->first].end();
			while (cur1 != fin1 && cur1->first != i)
				cur1++;
			if (cur1 == fin1)
				return false;
			cur++;
		}
		//All from reversed exist in straight
		cur = adjacencyLists_reversed[i].begin(), fin = adjacencyLists_reversed[i].end();
		while (cur != fin)
		{
			auto cur1 = adjacencyLists[cur->first].begin(), fin1 = adjacencyLists[cur->first].end();
			while (cur1 != fin1 && cur1->first != i)
				cur1++;
			if (cur1 == fin1)
				return false;
			cur++;
		}
	}
	return true;
}
