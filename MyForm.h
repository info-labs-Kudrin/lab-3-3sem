#pragma once

#include "Graph.h"
#include "FormerGraphForm.h"

#include <string>
#include <vector>

namespace Graphs {

	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	private:
		//Globals

		vector<int>* nodes_x, * nodes_y, *colors;
		Graph<int>* theGraph;
		FormerGraphForm frm;

	private: System::Windows::Forms::PictureBox^ pictureBox1;
	private: System::Windows::Forms::Button^ addNodeButton;

	private: System::Windows::Forms::Label^ changePositionLabel_N;

	private: System::Windows::Forms::Label^ changePositionLabel_X;

	private: System::Windows::Forms::Label^ changePositionLabel_Y;
	private: System::Windows::Forms::Button^ changePositionButton;
	private: System::Windows::Forms::GroupBox^ changePostitionGroupBox;
	private: System::ComponentModel::BackgroundWorker^ backgroundWorker1;
	private: System::Windows::Forms::GroupBox^ setEdgeGroupBox;
	private: System::Windows::Forms::Label^ setEdgeLabel_2;
	private: System::Windows::Forms::Button^ setEdgeButton;

	private: System::Windows::Forms::Label^ setEdgeLabel_1;

	private: System::Windows::Forms::Label^ setEdgeLabel_w;
	private: System::Windows::Forms::NumericUpDown^ changePositionNumericUpDown_Y;
	private: System::Windows::Forms::NumericUpDown^ changePositionNumericUpDown_X;
	private: System::Windows::Forms::NumericUpDown^ changePositionNumericUpDown_N;
	private: System::Windows::Forms::NumericUpDown^ setEdgeNumericUpDown_2;
	private: System::Windows::Forms::NumericUpDown^ setEdgeNumericUpDown_1;
	private: System::Windows::Forms::NumericUpDown^ setEdgeNumericUpDown_w;
	private: System::Windows::Forms::Button^ topSortButton;
	private: System::Windows::Forms::DataGridView^ topSortDataGridView;
	private: System::Windows::Forms::GroupBox^ removalGroupBox;
	private: System::Windows::Forms::NumericUpDown^ removeEdgeNumericUpDown_s;


	private: System::Windows::Forms::NumericUpDown^ removeEdgeNumericUpDown_f;
	private: System::Windows::Forms::Label^ removeEdgeLabel_s;
	private: System::Windows::Forms::Button^ removeEdgeButton;



	private: System::Windows::Forms::Label^ removeEdgeLabel_f;
	private: System::Windows::Forms::Button^ removeNodeButton;
	private: System::Windows::Forms::Label^ removeNodeLabel;
	private: System::Windows::Forms::NumericUpDown^ removeNodeNumericUpDown;
	private: System::Windows::Forms::Button^ markSCCButton;
	private: System::Windows::Forms::Button^ resetColorsButton;
	private: System::Windows::Forms::Button^ minVertexCoverButton;
	private: System::Windows::Forms::GroupBox^ dijkstraGroupBox;
	private: System::Windows::Forms::DataGridView^ dijkstraDataGridView;
	private: System::Windows::Forms::NumericUpDown^ dijkstraNumericUpDown_2;
	private: System::Windows::Forms::NumericUpDown^ dijkstraNumericUpDown_1;
	private: System::Windows::Forms::Label^ dijkstraLabel_2;
	private: System::Windows::Forms::Button^ dijkstraButton;
	private: System::Windows::Forms::Label^ dijkstraLabel_1;





	public:
		MyForm(void)
		{
			InitializeComponent();
			nodes_x = new vector<int>(0);
			nodes_y = new vector<int>(0);
			colors = new vector<int>(0);
			theGraph = new Graph<int>();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
			if (nodes_x)
				delete nodes_x;
			if (nodes_y)
				delete nodes_y;
			if (theGraph)
				delete theGraph;
			nodes_x = nodes_y = nullptr;
			theGraph = nullptr;
		}

	protected:

	protected:

	protected:

	protected:

	private:

		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->addNodeButton = (gcnew System::Windows::Forms::Button());
			this->changePositionLabel_N = (gcnew System::Windows::Forms::Label());
			this->changePositionLabel_X = (gcnew System::Windows::Forms::Label());
			this->changePositionLabel_Y = (gcnew System::Windows::Forms::Label());
			this->changePositionButton = (gcnew System::Windows::Forms::Button());
			this->changePostitionGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->changePositionNumericUpDown_Y = (gcnew System::Windows::Forms::NumericUpDown());
			this->changePositionNumericUpDown_X = (gcnew System::Windows::Forms::NumericUpDown());
			this->changePositionNumericUpDown_N = (gcnew System::Windows::Forms::NumericUpDown());
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->setEdgeGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->setEdgeNumericUpDown_2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->setEdgeNumericUpDown_1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->setEdgeNumericUpDown_w = (gcnew System::Windows::Forms::NumericUpDown());
			this->setEdgeLabel_2 = (gcnew System::Windows::Forms::Label());
			this->setEdgeButton = (gcnew System::Windows::Forms::Button());
			this->setEdgeLabel_1 = (gcnew System::Windows::Forms::Label());
			this->setEdgeLabel_w = (gcnew System::Windows::Forms::Label());
			this->topSortButton = (gcnew System::Windows::Forms::Button());
			this->topSortDataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->removalGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->removeNodeButton = (gcnew System::Windows::Forms::Button());
			this->removeNodeLabel = (gcnew System::Windows::Forms::Label());
			this->removeNodeNumericUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->removeEdgeNumericUpDown_s = (gcnew System::Windows::Forms::NumericUpDown());
			this->removeEdgeNumericUpDown_f = (gcnew System::Windows::Forms::NumericUpDown());
			this->removeEdgeLabel_s = (gcnew System::Windows::Forms::Label());
			this->removeEdgeButton = (gcnew System::Windows::Forms::Button());
			this->removeEdgeLabel_f = (gcnew System::Windows::Forms::Label());
			this->markSCCButton = (gcnew System::Windows::Forms::Button());
			this->resetColorsButton = (gcnew System::Windows::Forms::Button());
			this->minVertexCoverButton = (gcnew System::Windows::Forms::Button());
			this->dijkstraGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->dijkstraDataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->dijkstraNumericUpDown_2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->dijkstraNumericUpDown_1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->dijkstraLabel_2 = (gcnew System::Windows::Forms::Label());
			this->dijkstraButton = (gcnew System::Windows::Forms::Button());
			this->dijkstraLabel_1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->changePostitionGroupBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_Y))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_X))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_N))->BeginInit();
			this->setEdgeGroupBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_w))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->topSortDataGridView))->BeginInit();
			this->removalGroupBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeNodeNumericUpDown))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeEdgeNumericUpDown_s))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeEdgeNumericUpDown_f))->BeginInit();
			this->dijkstraGroupBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraDataGridView))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraNumericUpDown_2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraNumericUpDown_1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(10, 10);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(0);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(1024, 834);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm::pictureBox1_Paint);
			// 
			// addNodeButton
			// 
			this->addNodeButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->addNodeButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14));
			this->addNodeButton->Location = System::Drawing::Point(1044, 10);
			this->addNodeButton->Name = L"addNodeButton";
			this->addNodeButton->Size = System::Drawing::Size(410, 40);
			this->addNodeButton->TabIndex = 1;
			this->addNodeButton->Text = L"Add node!";
			this->addNodeButton->UseVisualStyleBackColor = false;
			this->addNodeButton->Click += gcnew System::EventHandler(this, &MyForm::addNodeButton_Click);
			// 
			// changePositionLabel_N
			// 
			this->changePositionLabel_N->AutoSize = true;
			this->changePositionLabel_N->ForeColor = System::Drawing::Color::Orange;
			this->changePositionLabel_N->Location = System::Drawing::Point(8, 57);
			this->changePositionLabel_N->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionLabel_N->Name = L"changePositionLabel_N";
			this->changePositionLabel_N->Size = System::Drawing::Size(74, 34);
			this->changePositionLabel_N->TabIndex = 3;
			this->changePositionLabel_N->Text = L"Number of the node";
			// 
			// changePositionLabel_X
			// 
			this->changePositionLabel_X->AutoSize = true;
			this->changePositionLabel_X->ForeColor = System::Drawing::Color::Coral;
			this->changePositionLabel_X->Location = System::Drawing::Point(88, 57);
			this->changePositionLabel_X->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionLabel_X->Name = L"changePositionLabel_X";
			this->changePositionLabel_X->Size = System::Drawing::Size(75, 34);
			this->changePositionLabel_X->TabIndex = 5;
			this->changePositionLabel_X->Text = L"New X coordinate";
			// 
			// changePositionLabel_Y
			// 
			this->changePositionLabel_Y->AutoSize = true;
			this->changePositionLabel_Y->ForeColor = System::Drawing::Color::Coral;
			this->changePositionLabel_Y->Location = System::Drawing::Point(174, 57);
			this->changePositionLabel_Y->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionLabel_Y->Name = L"changePositionLabel_Y";
			this->changePositionLabel_Y->Size = System::Drawing::Size(75, 34);
			this->changePositionLabel_Y->TabIndex = 7;
			this->changePositionLabel_Y->Text = L"New Y coordinate";
			// 
			// changePositionButton
			// 
			this->changePositionButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->changePositionButton->Location = System::Drawing::Point(255, 16);
			this->changePositionButton->Name = L"changePositionButton";
			this->changePositionButton->Size = System::Drawing::Size(148, 30);
			this->changePositionButton->TabIndex = 8;
			this->changePositionButton->Text = L"Change position!";
			this->changePositionButton->UseVisualStyleBackColor = false;
			this->changePositionButton->Click += gcnew System::EventHandler(this, &MyForm::changePositionButton_Click);
			// 
			// changePostitionGroupBox
			// 
			this->changePostitionGroupBox->Controls->Add(this->changePositionNumericUpDown_Y);
			this->changePostitionGroupBox->Controls->Add(this->changePositionNumericUpDown_X);
			this->changePostitionGroupBox->Controls->Add(this->changePositionNumericUpDown_N);
			this->changePostitionGroupBox->Controls->Add(this->changePositionLabel_Y);
			this->changePostitionGroupBox->Controls->Add(this->changePositionButton);
			this->changePostitionGroupBox->Controls->Add(this->changePositionLabel_X);
			this->changePostitionGroupBox->Controls->Add(this->changePositionLabel_N);
			this->changePostitionGroupBox->Location = System::Drawing::Point(1044, 56);
			this->changePostitionGroupBox->Name = L"changePostitionGroupBox";
			this->changePostitionGroupBox->Size = System::Drawing::Size(410, 104);
			this->changePostitionGroupBox->TabIndex = 9;
			this->changePostitionGroupBox->TabStop = false;
			this->changePostitionGroupBox->Text = L"Change position of a node";
			// 
			// changePositionNumericUpDown_Y
			// 
			this->changePositionNumericUpDown_Y->Location = System::Drawing::Point(172, 21);
			this->changePositionNumericUpDown_Y->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 540, 0, 0, 0 });
			this->changePositionNumericUpDown_Y->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_Y->MinimumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_Y->Name = L"changePositionNumericUpDown_Y";
			this->changePositionNumericUpDown_Y->Size = System::Drawing::Size(77, 22);
			this->changePositionNumericUpDown_Y->TabIndex = 10;
			// 
			// changePositionNumericUpDown_X
			// 
			this->changePositionNumericUpDown_X->Location = System::Drawing::Point(89, 21);
			this->changePositionNumericUpDown_X->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 740, 0, 0, 0 });
			this->changePositionNumericUpDown_X->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_X->MinimumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_X->Name = L"changePositionNumericUpDown_X";
			this->changePositionNumericUpDown_X->Size = System::Drawing::Size(77, 22);
			this->changePositionNumericUpDown_X->TabIndex = 9;
			// 
			// changePositionNumericUpDown_N
			// 
			this->changePositionNumericUpDown_N->Location = System::Drawing::Point(5, 21);
			this->changePositionNumericUpDown_N->MaximumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_N->MinimumSize = System::Drawing::Size(77, 0);
			this->changePositionNumericUpDown_N->Name = L"changePositionNumericUpDown_N";
			this->changePositionNumericUpDown_N->Size = System::Drawing::Size(77, 22);
			this->changePositionNumericUpDown_N->TabIndex = 0;
			// 
			// setEdgeGroupBox
			// 
			this->setEdgeGroupBox->Controls->Add(this->setEdgeNumericUpDown_2);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeNumericUpDown_1);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeNumericUpDown_w);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeLabel_2);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeButton);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeLabel_1);
			this->setEdgeGroupBox->Controls->Add(this->setEdgeLabel_w);
			this->setEdgeGroupBox->Location = System::Drawing::Point(1044, 166);
			this->setEdgeGroupBox->Name = L"setEdgeGroupBox";
			this->setEdgeGroupBox->Size = System::Drawing::Size(410, 104);
			this->setEdgeGroupBox->TabIndex = 10;
			this->setEdgeGroupBox->TabStop = false;
			this->setEdgeGroupBox->Text = L"Set edge";
			// 
			// setEdgeNumericUpDown_2
			// 
			this->setEdgeNumericUpDown_2->Location = System::Drawing::Point(172, 24);
			this->setEdgeNumericUpDown_2->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_2->MinimumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_2->Name = L"setEdgeNumericUpDown_2";
			this->setEdgeNumericUpDown_2->Size = System::Drawing::Size(77, 22);
			this->setEdgeNumericUpDown_2->TabIndex = 13;
			// 
			// setEdgeNumericUpDown_1
			// 
			this->setEdgeNumericUpDown_1->Location = System::Drawing::Point(89, 24);
			this->setEdgeNumericUpDown_1->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_1->MinimumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_1->Name = L"setEdgeNumericUpDown_1";
			this->setEdgeNumericUpDown_1->Size = System::Drawing::Size(77, 22);
			this->setEdgeNumericUpDown_1->TabIndex = 12;
			// 
			// setEdgeNumericUpDown_w
			// 
			this->setEdgeNumericUpDown_w->Location = System::Drawing::Point(5, 24);
			this->setEdgeNumericUpDown_w->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_w->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, System::Int32::MinValue });
			this->setEdgeNumericUpDown_w->MinimumSize = System::Drawing::Size(77, 0);
			this->setEdgeNumericUpDown_w->Name = L"setEdgeNumericUpDown_w";
			this->setEdgeNumericUpDown_w->Size = System::Drawing::Size(77, 22);
			this->setEdgeNumericUpDown_w->TabIndex = 11;
			// 
			// setEdgeLabel_2
			// 
			this->setEdgeLabel_2->AutoSize = true;
			this->setEdgeLabel_2->ForeColor = System::Drawing::Color::Coral;
			this->setEdgeLabel_2->Location = System::Drawing::Point(174, 57);
			this->setEdgeLabel_2->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeLabel_2->Name = L"setEdgeLabel_2";
			this->setEdgeLabel_2->Size = System::Drawing::Size(60, 34);
			this->setEdgeLabel_2->TabIndex = 7;
			this->setEdgeLabel_2->Text = L"Second node";
			// 
			// setEdgeButton
			// 
			this->setEdgeButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->setEdgeButton->Location = System::Drawing::Point(255, 19);
			this->setEdgeButton->Name = L"setEdgeButton";
			this->setEdgeButton->Size = System::Drawing::Size(148, 30);
			this->setEdgeButton->TabIndex = 8;
			this->setEdgeButton->Text = L"Set edge!";
			this->setEdgeButton->UseVisualStyleBackColor = false;
			this->setEdgeButton->Click += gcnew System::EventHandler(this, &MyForm::setEdgeButton_Click);
			// 
			// setEdgeLabel_1
			// 
			this->setEdgeLabel_1->AutoSize = true;
			this->setEdgeLabel_1->ForeColor = System::Drawing::Color::Coral;
			this->setEdgeLabel_1->Location = System::Drawing::Point(88, 57);
			this->setEdgeLabel_1->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeLabel_1->Name = L"setEdgeLabel_1";
			this->setEdgeLabel_1->Size = System::Drawing::Size(71, 17);
			this->setEdgeLabel_1->TabIndex = 5;
			this->setEdgeLabel_1->Text = L"First node";
			// 
			// setEdgeLabel_w
			// 
			this->setEdgeLabel_w->AutoSize = true;
			this->setEdgeLabel_w->ForeColor = System::Drawing::Color::Coral;
			this->setEdgeLabel_w->Location = System::Drawing::Point(8, 57);
			this->setEdgeLabel_w->MaximumSize = System::Drawing::Size(77, 0);
			this->setEdgeLabel_w->Name = L"setEdgeLabel_w";
			this->setEdgeLabel_w->Size = System::Drawing::Size(72, 34);
			this->setEdgeLabel_w->TabIndex = 3;
			this->setEdgeLabel_w->Text = L"Weight of the edge";
			// 
			// topSortButton
			// 
			this->topSortButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->topSortButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14));
			this->topSortButton->Location = System::Drawing::Point(1044, 386);
			this->topSortButton->Name = L"topSortButton";
			this->topSortButton->Size = System::Drawing::Size(410, 40);
			this->topSortButton->TabIndex = 11;
			this->topSortButton->Text = L"Sort topologically!";
			this->topSortButton->UseVisualStyleBackColor = false;
			this->topSortButton->Click += gcnew System::EventHandler(this, &MyForm::topSortButton_Click);
			// 
			// topSortDataGridView
			// 
			this->topSortDataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->topSortDataGridView->Location = System::Drawing::Point(1044, 432);
			this->topSortDataGridView->Name = L"topSortDataGridView";
			this->topSortDataGridView->RowHeadersWidth = 51;
			this->topSortDataGridView->RowTemplate->Height = 24;
			this->topSortDataGridView->Size = System::Drawing::Size(409, 78);
			this->topSortDataGridView->TabIndex = 12;
			// 
			// removalGroupBox
			// 
			this->removalGroupBox->Controls->Add(this->removeNodeButton);
			this->removalGroupBox->Controls->Add(this->removeNodeLabel);
			this->removalGroupBox->Controls->Add(this->removeNodeNumericUpDown);
			this->removalGroupBox->Controls->Add(this->removeEdgeNumericUpDown_s);
			this->removalGroupBox->Controls->Add(this->removeEdgeNumericUpDown_f);
			this->removalGroupBox->Controls->Add(this->removeEdgeLabel_s);
			this->removalGroupBox->Controls->Add(this->removeEdgeButton);
			this->removalGroupBox->Controls->Add(this->removeEdgeLabel_f);
			this->removalGroupBox->Location = System::Drawing::Point(1044, 276);
			this->removalGroupBox->Name = L"removalGroupBox";
			this->removalGroupBox->Size = System::Drawing::Size(410, 104);
			this->removalGroupBox->TabIndex = 14;
			this->removalGroupBox->TabStop = false;
			this->removalGroupBox->Text = L"Remove";
			// 
			// removeNodeButton
			// 
			this->removeNodeButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->removeNodeButton->Location = System::Drawing::Point(329, 21);
			this->removeNodeButton->Name = L"removeNodeButton";
			this->removeNodeButton->Size = System::Drawing::Size(74, 67);
			this->removeNodeButton->TabIndex = 15;
			this->removeNodeButton->Text = L"Remove node!";
			this->removeNodeButton->UseVisualStyleBackColor = false;
			this->removeNodeButton->Click += gcnew System::EventHandler(this, &MyForm::removeNodeButton_Click);
			// 
			// removeNodeLabel
			// 
			this->removeNodeLabel->AutoSize = true;
			this->removeNodeLabel->ForeColor = System::Drawing::Color::Coral;
			this->removeNodeLabel->Location = System::Drawing::Point(249, 46);
			this->removeNodeLabel->MaximumSize = System::Drawing::Size(77, 0);
			this->removeNodeLabel->Name = L"removeNodeLabel";
			this->removeNodeLabel->Size = System::Drawing::Size(74, 51);
			this->removeNodeLabel->TabIndex = 14;
			this->removeNodeLabel->Text = L"Number of removed node";
			// 
			// removeNodeNumericUpDown
			// 
			this->removeNodeNumericUpDown->Location = System::Drawing::Point(249, 21);
			this->removeNodeNumericUpDown->MaximumSize = System::Drawing::Size(77, 0);
			this->removeNodeNumericUpDown->MinimumSize = System::Drawing::Size(77, 0);
			this->removeNodeNumericUpDown->Name = L"removeNodeNumericUpDown";
			this->removeNodeNumericUpDown->Size = System::Drawing::Size(77, 22);
			this->removeNodeNumericUpDown->TabIndex = 11;
			// 
			// removeEdgeNumericUpDown_s
			// 
			this->removeEdgeNumericUpDown_s->Location = System::Drawing::Point(86, 21);
			this->removeEdgeNumericUpDown_s->MaximumSize = System::Drawing::Size(77, 0);
			this->removeEdgeNumericUpDown_s->MinimumSize = System::Drawing::Size(77, 0);
			this->removeEdgeNumericUpDown_s->Name = L"removeEdgeNumericUpDown_s";
			this->removeEdgeNumericUpDown_s->Size = System::Drawing::Size(77, 22);
			this->removeEdgeNumericUpDown_s->TabIndex = 13;
			// 
			// removeEdgeNumericUpDown_f
			// 
			this->removeEdgeNumericUpDown_f->Location = System::Drawing::Point(6, 21);
			this->removeEdgeNumericUpDown_f->MaximumSize = System::Drawing::Size(77, 0);
			this->removeEdgeNumericUpDown_f->MinimumSize = System::Drawing::Size(77, 0);
			this->removeEdgeNumericUpDown_f->Name = L"removeEdgeNumericUpDown_f";
			this->removeEdgeNumericUpDown_f->Size = System::Drawing::Size(77, 22);
			this->removeEdgeNumericUpDown_f->TabIndex = 12;
			// 
			// removeEdgeLabel_s
			// 
			this->removeEdgeLabel_s->AutoSize = true;
			this->removeEdgeLabel_s->ForeColor = System::Drawing::Color::Coral;
			this->removeEdgeLabel_s->Location = System::Drawing::Point(88, 49);
			this->removeEdgeLabel_s->MaximumSize = System::Drawing::Size(77, 0);
			this->removeEdgeLabel_s->Name = L"removeEdgeLabel_s";
			this->removeEdgeLabel_s->Size = System::Drawing::Size(60, 34);
			this->removeEdgeLabel_s->TabIndex = 7;
			this->removeEdgeLabel_s->Text = L"Second node";
			// 
			// removeEdgeButton
			// 
			this->removeEdgeButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->removeEdgeButton->Location = System::Drawing::Point(166, 21);
			this->removeEdgeButton->Name = L"removeEdgeButton";
			this->removeEdgeButton->Size = System::Drawing::Size(74, 67);
			this->removeEdgeButton->TabIndex = 8;
			this->removeEdgeButton->Text = L"Remove edge!";
			this->removeEdgeButton->UseVisualStyleBackColor = false;
			this->removeEdgeButton->Click += gcnew System::EventHandler(this, &MyForm::removeEdgeButton_Click);
			// 
			// removeEdgeLabel_f
			// 
			this->removeEdgeLabel_f->AutoSize = true;
			this->removeEdgeLabel_f->ForeColor = System::Drawing::Color::Coral;
			this->removeEdgeLabel_f->Location = System::Drawing::Point(2, 49);
			this->removeEdgeLabel_f->MaximumSize = System::Drawing::Size(77, 0);
			this->removeEdgeLabel_f->Name = L"removeEdgeLabel_f";
			this->removeEdgeLabel_f->Size = System::Drawing::Size(71, 17);
			this->removeEdgeLabel_f->TabIndex = 5;
			this->removeEdgeLabel_f->Text = L"First node";
			// 
			// markSCCButton
			// 
			this->markSCCButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->markSCCButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9));
			this->markSCCButton->Location = System::Drawing::Point(1044, 516);
			this->markSCCButton->Name = L"markSCCButton";
			this->markSCCButton->Size = System::Drawing::Size(249, 47);
			this->markSCCButton->TabIndex = 15;
			this->markSCCButton->Text = L"Mark strongly connected components!";
			this->markSCCButton->UseVisualStyleBackColor = false;
			this->markSCCButton->Click += gcnew System::EventHandler(this, &MyForm::markSCCButton_Click);
			// 
			// resetColorsButton
			// 
			this->resetColorsButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->resetColorsButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9));
			this->resetColorsButton->Location = System::Drawing::Point(1299, 516);
			this->resetColorsButton->Name = L"resetColorsButton";
			this->resetColorsButton->Size = System::Drawing::Size(151, 47);
			this->resetColorsButton->TabIndex = 16;
			this->resetColorsButton->Text = L"Reset colors!";
			this->resetColorsButton->UseVisualStyleBackColor = false;
			this->resetColorsButton->Click += gcnew System::EventHandler(this, &MyForm::resetColorsButton_Click);
			// 
			// minVertexCoverButton
			// 
			this->minVertexCoverButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->minVertexCoverButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9));
			this->minVertexCoverButton->Location = System::Drawing::Point(1118, 569);
			this->minVertexCoverButton->Name = L"minVertexCoverButton";
			this->minVertexCoverButton->Size = System::Drawing::Size(249, 47);
			this->minVertexCoverButton->TabIndex = 17;
			this->minVertexCoverButton->Text = L"Mark minimum vertex cover!";
			this->minVertexCoverButton->UseVisualStyleBackColor = false;
			this->minVertexCoverButton->Click += gcnew System::EventHandler(this, &MyForm::minVertexCoverButton_Click);
			// 
			// dijkstraGroupBox
			// 
			this->dijkstraGroupBox->Controls->Add(this->dijkstraDataGridView);
			this->dijkstraGroupBox->Controls->Add(this->dijkstraNumericUpDown_2);
			this->dijkstraGroupBox->Controls->Add(this->dijkstraNumericUpDown_1);
			this->dijkstraGroupBox->Controls->Add(this->dijkstraLabel_2);
			this->dijkstraGroupBox->Controls->Add(this->dijkstraButton);
			this->dijkstraGroupBox->Controls->Add(this->dijkstraLabel_1);
			this->dijkstraGroupBox->Location = System::Drawing::Point(1044, 622);
			this->dijkstraGroupBox->Name = L"dijkstraGroupBox";
			this->dijkstraGroupBox->Size = System::Drawing::Size(410, 171);
			this->dijkstraGroupBox->TabIndex = 18;
			this->dijkstraGroupBox->TabStop = false;
			this->dijkstraGroupBox->Text = L"Dijkstra";
			// 
			// dijkstraDataGridView
			// 
			this->dijkstraDataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dijkstraDataGridView->Location = System::Drawing::Point(6, 86);
			this->dijkstraDataGridView->Name = L"dijkstraDataGridView";
			this->dijkstraDataGridView->RowHeadersWidth = 51;
			this->dijkstraDataGridView->RowTemplate->Height = 24;
			this->dijkstraDataGridView->Size = System::Drawing::Size(397, 78);
			this->dijkstraDataGridView->TabIndex = 19;
			// 
			// dijkstraNumericUpDown_2
			// 
			this->dijkstraNumericUpDown_2->Enabled = false;
			this->dijkstraNumericUpDown_2->Location = System::Drawing::Point(86, 21);
			this->dijkstraNumericUpDown_2->MaximumSize = System::Drawing::Size(77, 0);
			this->dijkstraNumericUpDown_2->MinimumSize = System::Drawing::Size(77, 0);
			this->dijkstraNumericUpDown_2->Name = L"dijkstraNumericUpDown_2";
			this->dijkstraNumericUpDown_2->Size = System::Drawing::Size(77, 22);
			this->dijkstraNumericUpDown_2->TabIndex = 13;
			// 
			// dijkstraNumericUpDown_1
			// 
			this->dijkstraNumericUpDown_1->Location = System::Drawing::Point(6, 21);
			this->dijkstraNumericUpDown_1->MaximumSize = System::Drawing::Size(77, 0);
			this->dijkstraNumericUpDown_1->MinimumSize = System::Drawing::Size(77, 0);
			this->dijkstraNumericUpDown_1->Name = L"dijkstraNumericUpDown_1";
			this->dijkstraNumericUpDown_1->Size = System::Drawing::Size(77, 22);
			this->dijkstraNumericUpDown_1->TabIndex = 12;
			// 
			// dijkstraLabel_2
			// 
			this->dijkstraLabel_2->AutoSize = true;
			this->dijkstraLabel_2->ForeColor = System::Drawing::Color::Coral;
			this->dijkstraLabel_2->Location = System::Drawing::Point(88, 49);
			this->dijkstraLabel_2->MaximumSize = System::Drawing::Size(77, 0);
			this->dijkstraLabel_2->Name = L"dijkstraLabel_2";
			this->dijkstraLabel_2->Size = System::Drawing::Size(60, 34);
			this->dijkstraLabel_2->TabIndex = 7;
			this->dijkstraLabel_2->Text = L"Second node";
			// 
			// dijkstraButton
			// 
			this->dijkstraButton->BackColor = System::Drawing::Color::LightSeaGreen;
			this->dijkstraButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12));
			this->dijkstraButton->Location = System::Drawing::Point(169, 21);
			this->dijkstraButton->Name = L"dijkstraButton";
			this->dijkstraButton->Size = System::Drawing::Size(234, 45);
			this->dijkstraButton->TabIndex = 8;
			this->dijkstraButton->Text = L"Dijkstra!";
			this->dijkstraButton->UseVisualStyleBackColor = false;
			this->dijkstraButton->Click += gcnew System::EventHandler(this, &MyForm::dijkstraButton_Click);
			// 
			// dijkstraLabel_1
			// 
			this->dijkstraLabel_1->AutoSize = true;
			this->dijkstraLabel_1->ForeColor = System::Drawing::Color::Coral;
			this->dijkstraLabel_1->Location = System::Drawing::Point(6, 49);
			this->dijkstraLabel_1->MaximumSize = System::Drawing::Size(77, 0);
			this->dijkstraLabel_1->Name = L"dijkstraLabel_1";
			this->dijkstraLabel_1->Size = System::Drawing::Size(71, 17);
			this->dijkstraLabel_1->TabIndex = 5;
			this->dijkstraLabel_1->Text = L"First node";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Maroon;
			this->ClientSize = System::Drawing::Size(1462, 853);
			this->Controls->Add(this->dijkstraGroupBox);
			this->Controls->Add(this->minVertexCoverButton);
			this->Controls->Add(this->resetColorsButton);
			this->Controls->Add(this->markSCCButton);
			this->Controls->Add(this->removalGroupBox);
			this->Controls->Add(this->topSortDataGridView);
			this->Controls->Add(this->topSortButton);
			this->Controls->Add(this->setEdgeGroupBox);
			this->Controls->Add(this->changePostitionGroupBox);
			this->Controls->Add(this->addNodeButton);
			this->Controls->Add(this->pictureBox1);
			this->Location = System::Drawing::Point(-100, -100);
			this->MaximumSize = System::Drawing::Size(1480, 900);
			this->MinimumSize = System::Drawing::Size(1480, 900);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->changePostitionGroupBox->ResumeLayout(false);
			this->changePostitionGroupBox->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_Y))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_X))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->changePositionNumericUpDown_N))->EndInit();
			this->setEdgeGroupBox->ResumeLayout(false);
			this->setEdgeGroupBox->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->setEdgeNumericUpDown_w))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->topSortDataGridView))->EndInit();
			this->removalGroupBox->ResumeLayout(false);
			this->removalGroupBox->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeNodeNumericUpDown))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeEdgeNumericUpDown_s))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->removeEdgeNumericUpDown_f))->EndInit();
			this->dijkstraGroupBox->ResumeLayout(false);
			this->dijkstraGroupBox->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraDataGridView))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraNumericUpDown_2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dijkstraNumericUpDown_1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void pictureBox1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) {
		int n = nodes_x->size();
		SolidBrush^ drawBrush_text = gcnew SolidBrush(Color::Black);
		Drawing::Font^ drawFont = gcnew Drawing::Font("Arial", 12);
		StringFormat^ drawFormat = gcnew StringFormat();

		for (int i = 0; i < n; i++)
		{
			int xcur = (*nodes_x)[i], ycur = (*nodes_y)[i];
			SolidBrush^ drawBrush = gcnew SolidBrush(Color::FromArgb((*colors)[i]));
			e->Graphics->FillEllipse(drawBrush, xcur, ycur, 30, 30);

			String ^s = ""; int r = i;
			if (i == 0)
				s = "0";
			while (r != 0) {
				s = r % 10 + s;
				r /= 10;
			}

			e->Graphics->DrawString(s, drawFont, drawBrush_text, xcur+4, ycur+4, drawFormat);

			vector<pair<int, int>> cons = theGraph->getConnections(i);
			int sz = cons.size();
			SolidBrush^ drawBrush_text = gcnew SolidBrush(Color::LightSeaGreen);
			for (int j = 0; j < sz; j++)
			{
				int xsec = (*nodes_x)[cons[j].first], ysec = (*nodes_y)[cons[j].first];
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 14, ycur + 15, xsec + 14, ysec + 15);
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 15, ycur + 15, xsec + 15, ysec + 15);
				e->Graphics->DrawLine(Pens::LightSeaGreen, xcur + 15, ycur + 16, xsec + 15, ysec + 16);

				s = ""; r = cons[j].second;
				if (r == 0)
					s = "0";
				while (r != 0){
					s = r % 10 + s;
					r /= 10;
				}
				e->Graphics->DrawString(s, drawFont, drawBrush_text, xsec + 13 + (xcur - xsec) * 0.1, ysec + 11 + (ycur - ysec) * 0.1, drawFormat);
			}
		}
	}
private: System::Void addNodeButton_Click(System::Object^ sender, System::EventArgs^ e) {
	int x = rand() % 500, y = rand() % 500;
	(*nodes_x).push_back(x);
	(*nodes_y).push_back(y);
	(*colors).push_back(3506437888);
	theGraph->addNode();
	pictureBox1->Refresh();
}
private: System::Void changePositionButton_Click(System::Object^ sender, System::EventArgs^ e) {
	int cur = Convert::ToInt32(changePositionNumericUpDown_N->Value);
	int x = Convert::ToInt32(changePositionNumericUpDown_X->Value);
	int y = Convert::ToInt32(changePositionNumericUpDown_Y->Value);
	if (x < 0 || y < 0)
	{
		MessageBox::Show("A coordinate is less than 0", "Error");
		return;
	}
	if (cur >= (*nodes_x).size())
	{
		MessageBox::Show("Non-existent node", "Error");
		return;
	}
	(*nodes_x)[cur] = x;
	(*nodes_y)[cur] = y;
	pictureBox1->Refresh();
}
private: System::Void setEdgeButton_Click(System::Object^ sender, System::EventArgs^ e) {
	int weight = Convert::ToInt32(setEdgeNumericUpDown_w->Value);
	int nd1 = Convert::ToInt32(setEdgeNumericUpDown_1->Value);
	int nd2 = Convert::ToInt32(setEdgeNumericUpDown_2->Value);
	int n = nodes_x->size();
	if (nd2 >= n || nd1 >= n)
	{
		MessageBox::Show("Non-existent node", "Error");
		return;
	}
	theGraph->setEdge(nd1, nd2, weight);
	pictureBox1->Refresh();
}
private: System::Void topSortButton_Click(System::Object^ sender, System::EventArgs^ e) {
	frm.Activate();
	frm.Show();
	frm.setGraph(theGraph, nodes_x, nodes_y);

	if (theGraph->size() == 0)
		return;
	vector<unsigned int> nw;
	try {
		nw = theGraph->topologicallySort();
	}
	catch (exception e)
	{
		MessageBox::Show("A cycle was found. Topological sort is impossible!", "Error");
		return;
	}

	int n = nodes_x->size();

	vector<int>* nwx = new vector<int>(n), *nwy = new vector<int>(n);
	for (int i = 0; i < n; i++)
	{
		(*nwx)[nw[i]] = (*nodes_x)[i];
		(*nwy)[nw[i]] = (*nodes_y)[i];
	}
	delete nodes_x;
	delete nodes_y;
	nodes_x = nwx;
	nodes_y = nwy;
	pictureBox1->Refresh();

	
//	this->topSortDataGridView->Size = System::Drawing::Size(409, 96);
//	this->topSortDataGridView->AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders);
//	this->topSortDataGridView->AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode::AllCells;
//	topSortDataGridView->AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode::DisplayedCells;

	this->topSortDataGridView->RowHeadersWidth = 90;
	topSortDataGridView->RowCount = 1;
	topSortDataGridView->ColumnCount = n;
	topSortDataGridView->Rows[0]->Height = 30;
	topSortDataGridView->TopLeftHeaderCell->Value = "Former numbers";
	topSortDataGridView->Rows[0]->HeaderCell->Value = "New numbers";
	for (int i = 0; i < n; i++) {
		topSortDataGridView->Columns[i]->Width = 30;
		topSortDataGridView->Columns[i]->HeaderCell->Value = Convert::ToString(i);
		topSortDataGridView->Rows[0]->Cells[i]->Value = Convert::ToString(nw[i]);
	}
}
private: System::Void removeEdgeButton_Click(System::Object^ sender, System::EventArgs^ e) {
	unsigned int first = Convert::ToInt32(removeEdgeNumericUpDown_f->Value);
	unsigned int second = Convert::ToInt32(removeEdgeNumericUpDown_s->Value);
	if (first >= theGraph->size() || second >= theGraph->size())
	{
		MessageBox::Show("Non-exsistent node", "Error");
		return;
	}
	try
	{
		theGraph->removeEdge(first, second);
	}
	catch (const std::exception&)
	{
		return;
	}
	this->pictureBox1->Refresh();
}
private: System::Void removeNodeButton_Click(System::Object^ sender, System::EventArgs^ e) {
	unsigned int nd = Convert::ToInt32(removeNodeNumericUpDown->Value);
	if (nd >= theGraph->size())
	{
		MessageBox::Show("Non-existend node", "Error");
		return;
	}
	theGraph->removeNode(nd);
	nodes_x->erase(nodes_x->begin() + nd);
	nodes_y->erase(nodes_y->begin() + nd);
	colors->erase(colors->begin() + nd);
	this->pictureBox1->Refresh();
}
private: System::Void resetColorsButton_Click(System::Object^ sender, System::EventArgs^ e) {
	size_t sz = nodes_x->size();
	for (size_t i = 0; i < sz; i++)
		(*colors)[i] = 3506437888;
	this->pictureBox1->Refresh();
}
private: System::Void markSCCButton_Click(System::Object^ sender, System::EventArgs^ e) {
	vector<vector<unsigned int>> SCCs = theGraph->getSCC();
	for (int count = 0; count < SCCs.size(); count++)
	{
		unsigned int color = 128;
		for (int cnt = 0; cnt < 3; cnt++)
		{
			color = (color << 8);
			int cur = rand() % 256;
			color += cur;
		}
		for (int i = 0; i < SCCs[count].size(); i++)
			(*colors)[SCCs[count][i]] = color;
	}
	this->pictureBox1->Refresh();
}
private: System::Void minVertexCoverButton_Click(System::Object^ sender, System::EventArgs^ e) {
	vector<unsigned int> a = theGraph->getMinimumVertexCover();
	size_t sz = nodes_x->size();
	for (size_t i = 0; i < sz; i++)
		(*colors)[i] = 3506437888;
	unsigned int color = 4043309055;
	for (int i = 0; i < a.size(); i++)
		(*colors)[a[i]] = color;
	this->pictureBox1->Refresh();
}
private: System::Void dijkstraButton_Click(System::Object^ sender, System::EventArgs^ e) {
	unsigned int first = Convert::ToInt32(dijkstraNumericUpDown_1->Value);
	unsigned int second= Convert::ToInt32(dijkstraNumericUpDown_2->Value);
	vector<unsigned int> d;
	size_t n = theGraph->size();
	if (first >= n || second >= n)
	{
		MessageBox::Show("Non-existend node", "Error");
		return;
	}
	try
	{
		d = theGraph->Dijkstra(first, second).first;
	}
	catch (const std::exception&)
	{
		MessageBox::Show("Negative edges found", "Error");
		return;
	}

	dijkstraDataGridView->RowHeadersWidth = 90;
	dijkstraDataGridView->RowCount = 1;
	dijkstraDataGridView->ColumnCount = n;
	dijkstraDataGridView->Rows[0]->Height = 30;
	dijkstraDataGridView->TopLeftHeaderCell->Value = "Node";
	dijkstraDataGridView->Rows[0]->HeaderCell->Value = "Distance";
	for (int i = 0; i < n; i++) {
		dijkstraDataGridView->Columns[i]->Width = 30;
		dijkstraDataGridView->Columns[i]->HeaderCell->Value = Convert::ToString(i);
		if (d[i] != INT_MAX)
			dijkstraDataGridView->Rows[0]->Cells[i]->Value = Convert::ToString(d[i]);
		else
			dijkstraDataGridView->Rows[0]->Cells[i]->Value = "oo";
	}
}
};
}
